import socket
import time

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ip = '0.0.0.0'
port = 45722
client.connect((ip, port))

size = 2048
message = "Pineapple Paradaise"

client.send(message)
time1 = time.strftime('%H:%S:%M')

data = client.recv(size)
time2 = time.strftime('%H:%S:%M')
print "Time sent: ", time1
print "Time received: ", time2
print "The message received: ",data
client.close()
