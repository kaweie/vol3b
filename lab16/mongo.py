"""
<Name> James Longstaff
<Date> 3/7/2017
<Lab> Mongo
"""

import pymongo
import json
from pymongo import MongoClient
import re
import numpy as np
import itertools

### Problems ###
mc = MongoClient(port= 12345)
db = mc.db1
rest = db.collection1
def prob1():

	with open('restaurants.json') as data:
		for line in data:
			d = json.loads(line)
			rest.insert(d)
	print rest.find({'closing_time':'1800'}).count()
	#print list(rest)
def prob2():
	#rest = prob1()
	with open('mylans_bistro.json') as data:
		for line in data:
			print line
			d = json.loads(line)
			rest.insert(d)
	r = rest.find({'closing_time':'1800'})
	#print list(r)
def prob3():
	#This gets the number of restautrants in Manhattan
	num_rest_manhattan = rest.find({"borough":"Manhattan"}).count()
	#print num_rest_manhattan
	#Possible Grades
	A = rest.find({"grades.grade":"A"}).count()


	northern = list(rest.find().sort("address.coord.1",-1))[0:10]
	northern_names = [entry['name'] for entry in northern]

	#print np.sort(northern_names)
	all_rest = list(rest.find())
	#print len(all_rest)
	Names_rest = [rst['name'] for rst in all_rest]
	grill_rest = [name for name in Names_rest if bool(re.match(r"[\w\d'&.,/ ]*[gG][r][i][l][l][\w\d'&.,/ ]*",name)) == True]
	
	return num_rest_manhattan,A,northern_names,grill_rest

def prob4():
	all_rest = list(rest.find())
	
	#This section replaces the word 'grill' with 'magical fire table'.
	Names_rest = [rst['name'] for rst in all_rest]
	grill_rest = [name for name in Names_rest if bool(re.match(r"[\w\d'&.,/ ]*[gG][r][i][l][l][\w\d'&.,/ ]*",name)) == True]

	rst_grill_rest = [rst for rst in all_rest if rst['name'] in grill_rest]

	for name,rst in itertools.izip(grill_rest,rst_grill_rest):


		if bool(re.match(r"[\w\d'&., ]*[gG][rR][iI][lL][lL][\w\d'&., ]*",name)) == True:
			rest.update_one({'name':rst['name']},{'$set':{'name':rst['name'].replace("Grill","Magical Fire Table")}})

	
	#This section increases all of the restaurant IDs by 1000.
	for rst in all_rest:
		#print list(rest.find({"restaurant_id":rst["restaurant_id"]})),"\n\n"
		rest.update_one({"restaurant_id":rst["restaurant_id"]},{'$set':{"restaurant_id":str(int(rst["restaurant_id"]) + 1000)}})
		#print list(rest.find({"restaurant_id":str(int(rst["restaurant_id"])+1000)}))
		#print "\n\n\n\n\n"
	#This section deletes the entries that got C or below
	#print rest.find({"grades.grade":"C"}).count()
	rest.delete_many({"grades.grade":"C"})
	#print rest.find({"grades.grade":"C"}).count(), "\n\n\n"
	


	
def test():
	prob3()
