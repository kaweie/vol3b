"""Volume 3B: Web Scraping 1. Spec file."""
from bs4 import BeautifulSoup
import re

html_doc = """
    <html><head><title>The Three Stooges</title></head>
    <body>
    <p class="title"><b>The Three Stooges</b></p>
    <p class="story">Have you ever met the three stooges? Their names are
    <a href="http://example.com/larry" class="stooge" id="link1">Larry</a>,
    <a href="http://example.com/mo" class="stooge" id="link2">Mo</a> and
    <a href="http://example.com/curly" class="stooge" id="link3">Curly</a>;
    and they are really hilarious.</p>
    <p class="story">...</p>
    """

# Problem 1
def prob1(filename):
	"""
	Find all of the tags used in a particular .html file and the value of
	the 'type' attribute.

	Inputs:
		filename: Name of .html file to parse

	Outputs:
		- A set of all of the tags used in the .html file
		- The value of the 'type' attributes for the style tag
	"""
	tags_used = ['html','head','title','div','body','meta','style','h1','p','a'] # Make sure to return this is a s
	tags_used = set(tags_used)
	value = 'text/css'
	return tags_used, value

# Problem 2
def prob2():
	"""Prints (not returns) the prettified
	string for the Three Stooges HTML.
	"""
	soup = BeautifulSoup(html_doc,'html.parser')
	print soup.prettify()

# Problem 3
def prob3():
	"""Returns [u'title'] from the Three Stooges soup"""
	soup = BeautifulSoup(html_doc,'html.parser')
	tag = soup.title
	#print tag.attrs
	return tag.string

# Problem 4
def prob4():
	"""Returns u'Mo' from the Three Stooges soup"""
	soup = BeautifulSoup(html_doc,'html.parser')
	tag = soup.p
	children = tag.next_sibling.next_sibling.contents
	#vals = [val for val in tag if val != '\n']
	#print vals
	return children[3].string
# Problem 5
def prob5(method):
	"""Returns the u'More information...' using two different methods.
	If method is 1, it uses first method. If method is 2, it uses
	the second method.
	"""
	example_soup = BeautifulSoup(open('example1.htm'),'html.parser')
	if method == 1:
		return example_soup.p.next_sibling.next_sibling.string
	if method == 2:
		return example_soup.head.next_sibling.next_sibling.p.next_sibling.next_sibling.string

# Problem 6
#This is Problem 6
def prob6(method):
	"""Returns the tag associated with the "More information..."
	link using two different methods. If method is 1, it uses the
	first method. If method is 2, it uses the second method.
	"""
	example_soup = BeautifulSoup(open('example1.htm'),'html.parser')
	if method == 1:
		return example_soup.find(href = "http://www.iana.org/domains/example")
	if method == 2:
		return example_soup.find('p').next_sibling.next_sibling.a

# Problem 7
def prob7():
	"""Loads 'SanDiegoWeather.htm' into BeautifulSoup and prints
	(not returns) the tags referred to the in the Problem 7 questions.
	"""
	SD_soup = BeautifulSoup(open('SanDiegoWeather.htm'),'html.parser')
	# Question 1
	print "This is the tag for 'Thursday, January 1, 2015': ", SD_soup.find(text=re.compile("Thursday, January 1, 2015")).parent
	# Question 2
	print "This is the tag for 'Previous Day': ", SD_soup.find(text=re.compile("Previous Day")).parent
	print "This is the tag for 'Next Day: ",  SD_soup.find(text=re.compile("Next Day")).parent
	# Question 3
	print "This is the tag for 'Max Temperature': ", SD_soup.find(text="78").parent

# Problem 8examples are techniques that can aid you in your search for specific

def prob8():
	"""Loads 'Big Data dates.htm' into BeautifulSoup and uses find_all()
	and re to return a list of all tags containing links to bank data
	from September 30, 2003 to December 31, 2014.
	"""
	fed_soup = BeautifulSoup(open('BigDatadates.htm'),'html.parser')
	return fed_soup.find_all(href = re.compile(r"(20030930)|(20031[012]\d*)|(200[4-9]\d*)|(201[0-4]\d*)"))

