from mpi4py import MPI
from sys import argv
import numpy as np
import numpy.linalg as la
from mpi4py.MPI import ANY_SOURCE
"""
Write a script that runs a Monte Carlo simulation to approximate the volume
of an m-dimensional unit sphere.  Your script should accept the following
parameters as command line arguments:
    n - number of processes (passed as normal into mpirun)
    m - dimension of unit sphere
    p - number of points to use in simulation

For your final answer, print out the approximated volume from your simulation.
"""
def factorial(n):
    if n == 0:
        return 1
    else:
        return n * factorial(n-1)
COMM = MPI.COMM_WORLD
RANK = COMM.Get_rank()
SIZE = COMM.Get_size()
#print SIZE
m,p = int(argv[1]),int(argv[1])
n = SIZE + 1
s = 0
total = 0
area_cube = (2.)**n
vol = 0
if RANK != 0:
	print RANK
	print p
	print m
	dims = np.array([np.random.uniform(-1.0,1.0) for k in xrange(p)])
	for i in xrange(n):
		if la.norm(dims)**2 <= 1:
			s += 1
	
	vol = float(s)/n * area_cube 
	COMM.Send(vol,0)
elif RANK == 0:
	SOURCE = MPI.ANY_SOURCE
	COMM.Recv(vol,SOURCE)
	print vol

