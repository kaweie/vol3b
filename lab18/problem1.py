from mpi4py import MPI

"""
Write a program that prints "Hello from processor ____" for even processors
and "Goodbye from processor ____" for odd processors
"""
COMM = MPI.COMM_WORLD
RANK = COMM.Get_rank()

if RANK%2 == 0:
	print "Hello processor number {}.".format(RANK)
else:
	print "Goodbye processor number {}.".format(RANK)
