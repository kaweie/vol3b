from mpi4py import MPI
from sys import argv
import numpy as np

"""
Write a script that runs on n processes and passes an n by 1 array
of random values from the ith process to the i+1th process (or to the 0th
process for the if i is the last process).  Have each process identify
itself and what it is doing:

    e.g. "Process {process_num} sending array {array to process_num + 1}"
    and "Process {process_num} just received {array from process_num - 1}"
"""
COMM = MPI.COMM_WORLD
RANK = COMM.Get_rank()
SIZE = COMM.Get_size()

if RANK == (SIZE-1):
	rand_num = np.random.rand(1)
	a = rand_num
	print RANK
	COMM.Send(a,0)
	COMM.Recv(a,RANK-1)
else:
	print RANK
	rand_num = np.random.rand(1)
	a = rand_num
	COMM.Send(a,RANK+1)
	COMM.Recv(a,RANK-1)
