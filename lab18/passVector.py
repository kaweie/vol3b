from mpi4py import MPI
from sys import argv
import numpy as np


"""
Write a script that runs on two processes and passes an n by 1 array
of random values from one process to the other.  Have each process identify
itself and what it is doing:

    e.g. "Process 1 sending array {array}"
    and "Process 0 just received {array}"
"""

# Get n passed in by user
#print "Please type an interger to send: "
n = int(argv[1])
COMM = MPI.COMM_WORLD
RANK = COMM.Get_rank()
a=np.zeros(n, dtype=int)
if RANK == 0:
	vec = np.random.rand(n)
	a = vec
	COMM.Send(a, dest=1)
if RANK == 1:
	COMM.Recv(a, source=0)
	print a
