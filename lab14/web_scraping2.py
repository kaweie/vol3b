"""Volume III: Web Scraping 2.
<Name> James Longstaff
<Class> Math 405
<Date> 
"""
from bs4 import BeautifulSoup
import pandas as pd
import urllib2
import re
import matplotlib.pyplot as plt
import numpy as np
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
# Problem 1
def Prob1():
    """Load the Big Bank Info file and return a pandas DataFrame
    containing Bank Name, Rank, ID, Domestic Assets, and Domestic Branches
    for JP Morgan, Capital One, and Discover banks.
    """
    bank_soup = BeautifulSoup(open('federalreserve.htm'),'html.parser')
    tags = bank_soup.tbody.find_all('tr')
    rows = []
    for row in tags:
        pieces = []
        data = row.find_all('td')
        for piece in data:
            pieces.append(piece.string)
        rows.append(pieces)
    print rows[0]
    header = ['Bank Name','Rank','Bank ID','Location','Charter','Consol Assets','Domestic Assets','Pct Domestic Assets','Pct Cumulative Assets','Domestic Branches','Foreign Branches','IBF','Pct Foreign Owned']
    df = pd.DataFrame(rows,columns = header)
    mask = df.loc[(df['Bank Name'] == 'JPMORGAN CHASE BK/J P MORGAN CHASE & CO')| (df['Bank Name'] == 'DISCOVER BK/MORGAN STANLEY')|(df['Bank Name'] == 'CAPITAL ONE BK/')]
    return mask[['Bank Name','Rank','Bank ID','Domestic Assets','Domestic Branches',]]

# Problem 2
def Prob2():
	"""Use urllib2 and BeautifulSoup to return the actual max temperature,
	the tag containing the link for 'Next Day', and the url associated
	with that link.
	"""
	weath_url = 'https://math.byu.edu/weather/www.wunderground.com/history/airport/KSLC/2015/1/1/DailyHistory.html?req_city=&req_state=&req_statename=&reqdb.zip=&reqdb.magic=&reqdb.wmo='
	weath_content = urllib2.urlopen(weath_url).read()
	weath_soup = BeautifulSoup(weath_content,'html.parser')
	tags = weath_soup.tbody.find_all('tr')
	trs = [tag.td.string == u'Max Temperature' for tag in tags]
	vals = [i for i, x in enumerate(trs) if x]
	true_vals = tags[vals[0]]
	Max_Temp = true_vals.td.next_sibling.next_sibling.text
	tag_NextDay = weath_soup.find(class_='next-link').a
	url = tag_NextDay["href"]
	return Max_Temp,url
# Problem 3
def Prob3():
	"""Mimic the Wunderground Weather example to create a list of average
	max temperatures of the year 2015 in San Diego. Use matplotlib to draw
	a graph depicting the data, then return the list.
	"""
	weather_url = 'https://math.byu.edu/weather/www.wunderground.com/history/airport/KSLC/2015/1/1/DailyHistory.html?req_city=&req_state=&req_statename=&reqdb.zip=&reqdb.magic=&reqdb.wmo='
	weather_content = urllib2.urlopen(weather_url).read()
	weather_soup = BeautifulSoup(weather_content,'html.parser')
	#print weather_soup.text
	actual = []
	months = ['January','February']
	for i, month in enumerate(months):
		while('2016'not in weather_soup.find(class_='history-date').text and month in weather_soup.find(class_='history-date').text):
			#print weather_soup.find(class_='history-date').text
			if(len(weather_soup.find_all(string='Actual')) != 1):
				weather_content = urllib2.urlopen(weather_url).read()
				weather_soup = BeautifulSoup(weather_content)
			#print weather_soup.find(text='Max Temperature')
			actual_temp = weather_soup.find(text='Max Temperature').parent.parent.next_sibling.next_sibling.span.span.text
			actual.append(int(actual_temp))
			#print actual
			next_url = weather_soup.find(text = re.compile('Next Day')).parent['href']
			#print next_url
			weather_url ='https://math.byu.edu/weather/www.wunderground.com/history/airport/KSLC/2015/' + str(i+1) +'/1/'+next_url
			#print weather_url
			weather_content = urllib2.urlopen(weather_url)
			weather_soup = BeautifulSoup(weather_content.read())
	plt.plot(actual)
	plt.title("Max Temperatures in SLC from 1/1/2015 - 2/28/2015")
	plt.xlabel("Days")
	plt.ylabel("Degrees Farenheit")
	plt.show()
	return actual
	#raise NotImplementedError("Problem 3 not complete")

# Problem 4
def Prob4():
	espn_url = "http://www.espn.com/nba/statistics"
	espn_content = urllib2.urlopen(espn_url).read()
	espn_soup = BeautifulSoup(espn_content,'html.parser')
	dog = espn_soup.find_all('tr')#.find(class_ = "span-2").contents#.find_all('tr')
	
	#dog[1].text = (dog[1].text)[0:7] + ' ' + (dog[1].text)[7::]
	#print dog[2].text
	#print dog[3].text

	#print len(dog)
	#print dog
	base_url = 'http://www.espn.com/nba/player/'
	frames = []
	for i in xrange(1,6):
		#print "New Round!",'\n\n\n'
		player_url = dog[i].a['href']
		player_content = urllib2.urlopen(player_url)
		player_soup = BeautifulSoup(player_content.read())


		table = player_soup.find(class_ = 'mod-container mod-table mod-no-footer')
		headers = np.array([item.text for item in table.find_all('th')])
		
		headers = np.insert(headers[1::],0,"Name")
		headers = [headers[0],headers[1],headers[2],headers[-1],headers[4]]
		print headers
		career_stats = np.array([item.text for item in table.find_all('td')])
		career_stats = np.insert(career_stats[17::],0,dog[i].text)
		if i == 3:
			career_stats[0] = (career_stats[0])[2::]
			career_stats[0] = (career_stats[0])[0:-6]
			career_stats[0] = (career_stats[0])[0:-1]
		elif i != 1:
			career_stats[0] = (career_stats[0])[2::]
			career_stats[0] = (career_stats[0])[0:-6]
		else:
			career_stats[0] = (career_stats[0]).replace('\n','')
			career_stats[0] = (career_stats[0])[0:6] + ' ' + (career_stats[0])[7::]
		career_stats = [career_stats[0],career_stats[1],career_stats[2],career_stats[-1],career_stats[4]]
		frames.append(career_stats)
		
	frames = np.array(frames).T
	print frames
	#print headers
	#print np.shape(frames)
	data_frames = pd.DataFrame(data=frames,index = headers)
	
	return data_frames

# Problem 5
def Prob5():
	"""Use selenium to return a list of all the a tags containing each of the
	30 NBA teams. Return only one tag per team.
	"""
	driver = webdriver.Firefox()
	url = "http://stats.nba.com/teams/traditional/#!?sort=W_PCT&dir=-1"
	driver.get(url)

	nba_soup = BeautifulSoup(driver.page_source)

	
	#target.write(str(nba_soup.prettify()))
	dog = nba_soup.find_all(class_='ng-binding')
	teams = np.array([element for element in dog])
	teams = teams[-59::2]
	return teams
	#raise NotImplementedError("Problem 5 not complete")
