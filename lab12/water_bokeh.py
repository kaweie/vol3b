from bokeh.plotting import figure,output_file, show
from bokeh.models import WMTSTileSource, ColumnDataSource
from pyproj import Proj, transform
import requests
import numpy as np
from bokeh.io import curdoc
from bokeh.layouts import column, row, layout, widgetbox
import pandas as pd


from_proj = Proj(init="epsg:4326")
to_proj = Proj(init="epsg:3857")

def convert(longitudes, latitudes):
    x_vals = []
    y_vals = []
    for lon, lat in zip(longitudes, latitudes):
        x, y = transform(from_proj, to_proj, lon, lat)
        x_vals.append(x)
        y_vals.append(y)
    return x_vals, y_vals

fig = figure(title="Los Angeles Water Usage 2012-2013", plot_width=600,
                plot_height=600, tools=["wheel_zoom", "pan"],
                x_range=(-13209490, -13155375), y_range=(3992960, 4069860),
                webgl=True, active_scroll="wheel_zoom")
fig.axis.visible = False
STAMEN_TONER_BACKGROUND = WMTSTileSource(
    url='http://tile.stamen.com/toner-background/{Z}/{X}/{Y}.png',
    attribution=(
        'Map tiles by <a href="http://stamen.com">Stamen Design</a>, '
        'under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>.'
        'Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, '
        'under <a href="http://www.openstreetmap.org/copyright">ODbL</a>'
    )
)
background = fig.add_tile(STAMEN_TONER_BACKGROUND)

# Do whatever you need to in order to display the figure here

add = 'https://data.lacity.org/resource/v87k-wgde.json'
df = pd.read_json(add)
length = len(df.index)
lons = [df['location_1'][i]['coordinates'][0] for i in xrange(length)]
lats = [df['location_1'][i]['coordinates'][1] for i in xrange(length)]
water_vals = df['fy_12_13']
x_vals,y_vals = convert(lons,lats)

df_meters = pd.DataFrame({'x_vals': x_vals,
			'y_vals': y_vals,
			'water': water_vals})

fig.circle(x=df_meters['x_vals'], y=df_meters['y_vals'],fill_alpha = 0.5,size = df_meters['water'])
show(fig)
