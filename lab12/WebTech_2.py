import json
import datetime
import requests
import pandas as pd
import numpy as np
import xml.etree.ElementTree as et

from pyproj import Proj, transform
from scipy.spatial import cKDTree

# Problem 1
class DateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
        pass

def DateTimeDecoder(item):
	type = accepted_dtypes.get(item['dytpe'],None)
	if type is not None and 'data' in item:
		return type(item['data'])
	return item

# Problem 2
# Provide a solution to this problem in a separate file



# Problem 3
def prob3():
	# Your code to get the answers here

	f = et.iterparse('books.xml')
	#root = f.getroot()
	#print root.text
	maxP = 0
	author_expensive = ""
	author = 'donkey'
	count_date = 0
	count_desc = 0
	for event, tag in f:
		
		#print "{}:{}".format(tag.tag, tag.text)
		#Part I
		if tag.tag == 'author':
			author = tag.text

		if tag.tag == 'price':
			if maxP < float(tag.text):

				maxP = float(tag.text)
				author_expensive = author

		#Part II

		if tag.tag == 'publish_date':
			year = int(tag.text[0:4])
			#print year
			month = tag.text[5:7]
			#print int(month)
			if month[0] == 0:
				month = month[1]

			if year == 2000:
				#print "here!"
				if float(month) < 12:
					#print"there!"
					count_date += 1
					#print "cat!"
			elif year < 2000:
				count_date+=1

		#Part III
		if tag.tag == 'description':
			desc = tag.text
			arr = np.array(desc.split(' '))
			#print arr
			bag = np.array(set(arr.flatten()))
			print bag
			if 'Microsoft' in bag:
				count_desc +=1
					

		tag.clear()
	"""
	maxP = 0
	for n in root.iter('price'):
		if maxP < n:
			maxP = n
	print maxP.text
	"""
	print "The author with the most expensive book is", author_expensive
	print "The number of books published before Dec 1, 2000 is", count_date
	print "The books that reference Microsoft in their description are", count_desc


# Problem 4
def convert(longitudes, latitudes):
    from_proj = Proj(init="epsg:4326")
    to_proj = Proj(init="epsg:3857")

    x_vals = []
    y_vals = []
    for lon, lat in zip(longitudes, latitudes):
        x, y = transform(from_proj, to_proj, lon, lat)
        x_vals.append(x)
        y_vals.append(y)

    return x_vals, y_vals

def prob4():
    pass

if __name__ == "__main__":
    pass
