# parallel1.py
import numpy as np
from ipyparallel import Client
import time
import matplotlib.pyplot as plt
client = Client()
dview = client[:]

def prob3(n=1000000):
	"""
	Write a function that returns the mean, max, and min for n draws
	from the standard normal distribution where n is default to 1,000,000.
	Use apply_sync() to execute this function across all available engines.
	Print the results as returned by each engine.
	"""
	samples = np.random.randn(n)
	#print samples
	def get_mean(samples):
		import numpy as np
		return np.mean(samples)
	def get_max(samples):
		import numpy as np
		return np.max(samples)
	def get_min(samples):
		import numpy as np
		return np.min(samples)

	means = dview.apply_sync(get_mean,samples)
	maxs = dview.apply_sync(get_max,samples)
	mins = dview.apply_sync(get_min,samples)
	return means,maxs,mins

def prob4():
	"""
	Time the function from the previous problem in parallel and serially. Run
	apply_sync() on the function to time in parallel. To time the function
	serially, run the function in a for loop n times, where n is the number
	of engines on your machine. Print the results.
	"""
	def serial(n):
		samples = np.random.randn(n)
		def get_mean(samples):
			import numpy as np
			return np.mean(samples)
		def get_max(samples):
			import numpy as np
			return np.max(samples)
		def get_min(samples):
			import numpy as np
			return np.min(samples)
		means = []
		maxs = []
		mins = []
		for i in xrange(len(client.ids)):
			means.append(get_mean(samples))
			maxs.append(get_max(samples))
			mins.append(get_min(samples))
		
		return means,maxs,mins
	diffs_para = []
	diffs_serial = []
	for n in [1000000, 5000000, 10000000,1500000]:
		start_para = time.time() #Calculates current time to nanosecond
		means, maxs,mins = prob3(n)
		timing_para = time.time() - start_para
		diffs_para.append(timing_para)
		
		start_serial = time.time() #Calculates current time to nanosecond
		means, maxs,mins = serial(n)
		timing_serial = time.time() - start_serial
		diffs_serial.append(timing_serial)
	print "The timings it took to run parallel processcors:", diffs_para, "\n\n"
	print "The timing it took to run serial processcors:", diffs_serial
	return diffs_para, diffs_serial


def prob5(N=10):
	"""
	Accept an integer N that represents the number of draws to take from the
	normal distribution for the distribution X. Take 500,000 draws from this
	distribution and plot a histogram of the results. Split the load evenly
	among all available engines and make the function flexible to the number
	of engines running.
	"""
	M = 500000/len(client.ids)
	def samps(N):
		import numpy as np
		X = np.random.randn(N)
		return X

	def get_maxes(M,N):
		maxes = [max(samps(N)) for i in xrange(M)]
		return maxes
	maxs = get_maxes(M,N)
	
	points = (np.array(dview.apply_sync(get_maxes,M,N))).flatten()
	plt.hist(points)
	plt.show()
	"""
	def get_Draws(N):
		import numpy as np
		import matplotlib.pyplot as plt
		
		plt.hist(draws)
		plt.show()

	M = 500000/len(client.ids)
	dview.apply_sync(get_Draws,K)
	"""
def parallel_trapezoidal_rule(f, a, b, n=200):
	"""
	Write a function that accepts a function handle, f, bounds of integration,
	a and b, and a number of points to use, n. Split the interval of
	integration evenly among all available processors and use the trapezoidal
	rule to numerically evaluate the integral over the interval [a,b].

	Parameters:
		f (function handle): the function to evaluate
		a (float): the lower bound of integration
		b (float): the upper bound of integration
		n (int): the number of points to use, defaults to 200
	Returns:
		value (float): the approximate integral calculated by the
			trapezoidal rule
	"""
	import numpy as np
	X = np.linspace(a,b,n)
	h = (X[2] - X[1])


	Diff = [f(X[i]) + f(X[i+1]) for i in xrange(len(X) - 1)]
	dview.scatter('Diff_partition',Diff)

	dview['h'] = h
	
	dview.execute("""

sum_Diff = h/2. *sum(Diff_partition)
""")
	values_list = dview.gather("sum_Diff",block=True)
	return sum(values_list)
	
if __name__ == "__main__":
	"""
	f = lambda x: x+2.
	a = 0.
	b = 1.
	hello = parallel_trapezoidal_rule(f, a, b, n=200)
	"""
	f = lambda x: np.exp(x-2)
	a = 0
	b = 3
	#prob5()
	#prob4()
	num = parallel_trapezoidal_rule(f,a,b,201)
	print num
