# test_specs.py
"""Python Essentials: Testing.
<Name> James Longstaff
<Class> Math 405
<Date> 2/28/2016
"""

import specs
import pytest
import numpy as np
from itertools import combinations
# Problem 1: Test the addition and smallest factor functions from specs.py
def test_addition():
	#General Additions
	assert specs.addition(2,3) == 5
	assert specs.addition(92,13) == 105

	#Positive Answers, different signs of inputs
	assert specs.addition(-1,4) == 3
	assert specs.addition(21,-9) == 12

	#Negative Answers, different signs of inputs
	assert specs.addition(-32,3) == -29
	assert specs.addition(34,-50) == -16

def test_smallest_factor():
	assert specs.smallest_factor(1) == 1
	assert specs.smallest_factor(3) == 3
	assert specs.smallest_factor(12) == 2
	assert specs.smallest_factor(24) == 2
	assert specs.smallest_factor(9) == 3
	assert specs.smallest_factor(25) == 5
	assert specs.smallest_factor(49) == 7
	assert specs.smallest_factor(169) == 13

# Problem 2: Test the operator function from specs.py
def test_operator():
	"""
	#I'm having some kind of problem with this.
	not_string = 10
	with pytest.raises(Exception) as excinfo:
		operator(2,3,not_string)
	assert excinfo.typename =='ValueError'
	assert excinfo.value.args[not_string] == "Oper should be a string"


	twoChar = "**"
	with pytest.raises(Exception) as excinfo:
		operator(10,5,twoChar)
	assert excinfo.typename =='ValueError'
	assert excinfo.value.args[twoChar] == "Oper should be one character"

	divisor = "/"
	with pytest.raises(Exception) as excinfo:
		operator(10,0,divisor)
	assert excinfo.typename =='ValueError'
	assert excinfo.value.args[0] == "You can't divide by zero!"

	gen_accept = "cat"
	with pytest.raises(Exception) as excinfo:
		operator(10,5,gen_accept)
	assert excinfo.typename =='ValueError'
	assert excinfo.value.args[gen_accept] == "Oper can only be: '+', '/', '-', or '*'"
	"""
	pytest.raises(ValueError, specs.operator, a=4, b=2, oper=10)
	pytest.raises(ValueError, specs.operator, a=4, b=2, oper='**')
	pytest.raises(ValueError, specs.operator, a=4, b=2, oper='a')
	assert specs.operator(4,2,'+')
	assert specs.operator(4,2,'/')
	assert specs.operator(4,2,'-')
	assert specs.operator(4,2,'*')
	pytest.raises(ValueError, specs.operator, a=4, b=0, oper='/')

	pytest.raises(ValueError, specs.operator, a=4, b=2, oper='cats')

# Problem 3: Finish testing the complex number class
@pytest.fixture
def set_up_complex_nums():
	number_1 = specs.ComplexNumber(1, 2)
	number_2 = specs.ComplexNumber(5, 5)
	number_3 = specs.ComplexNumber(2, 9)
	number_4 = specs.ComplexNumber(2,-3)
	return number_1, number_2, number_3,number_4

def test_complex_addition(set_up_complex_nums):
    number_1, number_2, number_3,number_4 = set_up_complex_nums
    assert number_1 + number_2 == specs.ComplexNumber(6, 7)
    assert number_1 + number_3 == specs.ComplexNumber(3, 11)
    assert number_2 + number_3 == specs.ComplexNumber(7, 14)
    assert number_3 + number_3 == specs.ComplexNumber(4, 18)

def test_complex_multiplication(set_up_complex_nums):
    number_1, number_2, number_3,number_4 = set_up_complex_nums
    assert number_1 * number_2 == specs.ComplexNumber(-5, 15)
    assert number_1 * number_3 == specs.ComplexNumber(-16, 13)
    assert number_2 * number_3 == specs.ComplexNumber(-35, 55)
    assert number_3 * number_3 == specs.ComplexNumber(-77, 36)

def test_complex_subtraction(set_up_complex_nums):
	number_1, number_2, number_3,number_4 = set_up_complex_nums
	assert number_1 - number_2 == specs.ComplexNumber(-4, -3)
	assert number_1 - number_3 == specs.ComplexNumber(-1, -7)
	assert number_2 - number_3 == specs.ComplexNumber(3, -4)
	assert number_3 - number_3 == specs.ComplexNumber(0, 0)

def test_complex_division(set_up_complex_nums):
	number_1, number_2, number_3,number_4 = set_up_complex_nums
	zero = specs.ComplexNumber(0,0)
	with pytest.raises(Exception) as excinfo:
		number_1/zero
	assert excinfo.typename == 'ValueError'
	assert excinfo.value.args[0] == "Cannot divide by zero"

	assert number_1 / number_2 == specs.ComplexNumber(.3,.1)
	assert number_1 / number_3 == specs.ComplexNumber(4./17,-1./17)
	assert number_2 / number_3 == specs.ComplexNumber(11./17,-7./17)
	assert number_3 / number_3 == specs.ComplexNumber(1,0)
	assert number_3 / number_4 == specs.ComplexNumber(-23./13, 24./13)
	"""
	assert number_1 / number_3 == specs.ComplexNumber(1./2, 2./9)
	assert number_2 / number_3 == specs.ComplexNumber(5./2, 5./9)
	assert number_3 / number_3 == specs.ComplexNumber(1, 1)
	"""
def test_complex_norm(set_up_complex_nums):
	number_1, number_2, number_3,number_4 = set_up_complex_nums
	assert number_1.norm()
	assert number_2.norm()
	assert number_3.norm()
	assert number_4.norm()
def test_complex_eq(set_up_complex_nums):
	number_1, number_2, number_3,number_4 = set_up_complex_nums
	assert number_1 == number_1
	assert number_2 == number_2
	assert number_3 == number_3

def test_complex_str(set_up_complex_nums):
	number_1, number_2, number_3,number_4 = set_up_complex_nums
	assert str(number_1) == '1+2i'
	assert str(number_2) == '5+5i'
	assert str(number_3) == '2+9i'
	assert str(number_4) == '2-3i'

# Problem 4: Write test cases for the Set game.
class Set:
	def __init__(self,color,pattern,shape,quantity):
		self.color = color
		self.pattern = pattern
		self.shape = shape
		self.quantity = quantity


def test_initialize_Set():
	assert specs.Set(0,1,3,2).color == Set(0,1,3,2).color
	assert specs.Set(0,1,3,2).pattern == Set(0,1,3,2).pattern
	assert specs.Set(0,1,3,2).shape == Set(0,1,3,2).shape
	assert specs.Set(0,1,3,2).quantity == Set(0,1,3,2).quantity

#THIS IS FAILING
def test_length12():
	#pytest.raises(ValueError, specs.play_set, filename="cards8.txt")
	with pytest.raises(Exception) as excinfo:
		specs.play_set("cards8.txt")
	print excinfo.typename
	assert excinfo.typename =='ValueError'

#THIS IS FAILING
def test_only_ints():
	#pytest.raises(ValueError, specs.play_set, filename="cards_q.txt")
	with pytest.raises(Exception) as excinfo:
		specs.play_set("cards_q.txt")
	assert excinfo.typename =='ValueError'
	#assert excinfo.value.args[0] == "Each character of each line of the input file must be an int."

#THIS IS FAILING
def test_greater_4_char():
	#pytest.raises(ValueError, specs.play_set, filename="cards_5char.txt")
	with pytest.raises(Exception) as excinfo:
		specs.play_set("cards_5char.txt")
	assert excinfo.typename =='ValueError'
	#assert excinfo.value.args[0] == "Each line in your input file should only have 4 characters."

#THIS IS FAILING
def test_less_4_char():
	with pytest.raises(Exception) as excinfo:
		specs.play_set("cards_3char.txt")
	assert excinfo.typename =='ValueError'

#THIS IS FAILING
def test_wrong_ints():
	#pytest.raises(ValueError, specs.play_set, filename="cardswrongint.txt")
	with pytest.raises(Exception) as excinfo:
		specs.play_set("cardswrongint.txt")
	assert excinfo.typename =='ValueError'
	#assert excinfo.value.args[0] == "The only acceptable input values are the int values are '0','1', or '2'."


def test_return():
	assert specs.play_set("cards.txt")
"""
def test_play_set():
	#This tests the initialization part of the Set Game.
	test_initialize_Set()
	test_length12()
	test_only_ints()
	test_4_char()
	test_wrong_ints()


	data = np.loadtxt("cards.txt",dtype = str)
	combos = list(combinations(data,3))
	#test_input_errors(data,combos)
	#Next we are going to test the outputs given to us from the specs function.
	num_of_sets,sets = specs.play_set("cards.txt")

	for s in sets:
		s1 = [int(s[0][i]) for i in xrange(len(s[0]))]
		s2 = [int(s[1][i]) for i in xrange(len(s[1]))]
		s3 = [int(s[2][i]) for i in xrange(len(s[2]))]
		
		card_1 = Set(s1[0],s1[1],s1[2],s1[3])
		card_2 = Set(s2[0],s2[1],s2[2],s2[3])
		card_3 = Set(s3[0],s3[1],s3[2],s3[3])
		
		assert (card_1.pattern+card_2.pattern+card_3.pattern)%3 == (card_1.color+card_2.color+card_3.color)%3 == (card_1.shape+card_2.shape+card_3.shape)%3 == (card_1.quantity+card_2.quantity+card_3.quantity)%3
"""
"""
#This next section tests input errors:
data = np.loadtxt("",dtype = str)
new_data = data[0:-1]
new_combos = list(combinations(new_data,3))
input_errors(new_data,new_combos)
"""
	
