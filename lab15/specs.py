# specs.py
"""Python Essentials: Testing.
EDIT FOR COMIT
<Name> James Longstaff
<Class> Math 404
<Date> 2/28/2017
"""
import math
import numpy as np
from itertools import combinations
# Problem 1 Write unit tests for addition().
# Be sure to install pytest-cov in order to see your code coverage change.
def addition(a,b):
    return a + b

def smallest_factor(n):
    """Finds the smallest prime factor of a number.
    Assume n is a positive integer.
    """
    if n == 1:
        return 1
    for i in range(2, int(math.sqrt(n)) + 1):
        if n % i == 0:
            return i
    return n


# Problem 2 Write unit tests for operator().
def operator(a, b, oper):
    if type(oper) != str:
        raise ValueError("Oper should be a string")
    if len(oper) != 1:
        raise ValueError("Oper should be one character")
    if oper not in ['+','-','*','/']:
        raise ValueError("Oper can only be: '+', '/', '-', or '*'")
    if oper == "+":
        return a+b
    if oper == "/":
        if b == 0:
            raise ValueError("You can't divide by zero!")
        return a/float(b)
    if oper == "-":
        return a-b
    if oper == "*":
        return a*b


# Problem 3 Write unit test for this class.
class ComplexNumber(object):
    def __init__(self, real=0, imag=0):
        self.real = real
        self.imag = imag

    def conjugate(self):
        return ComplexNumber(self.real, -self.imag)

    def norm(self):
        return math.sqrt(self.real**2 + self.imag**2)

    def __add__(self, other):
        real = self.real + other.real
        imag = self.imag + other.imag
        return ComplexNumber(real, imag)

    def __sub__(self, other):
        real = self.real - other.real
        imag = self.imag - other.imag
        return ComplexNumber(real, imag)

    def __mul__(self, other):
        real = self.real*other.real - self.imag*other.imag
        imag = self.imag*other.real + other.imag*self.real
        return ComplexNumber(real, imag)

    def __div__(self, other):
        if other.real == 0 and other.imag == 0:
            raise ValueError("Cannot divide by zero")
        bottom = (other.conjugate()*other*1.).real
        top = self*other.conjugate()
        return ComplexNumber(top.real / bottom, top.imag / bottom)

    def __eq__(self, other):
        return self.imag == other.imag and self.real == other.real

    def __str__(self):
        return "{}{}{}i".format(self.real, '+' if self.imag >= 0 else '-',
                                                                abs(self.imag))

# Problem 5: Write code for the Set game here
class Set:
	def __init__(self,color,pattern,shape,quantity):
		self.color = color
		self.pattern = pattern
		self.shape = shape
		self.quantity = quantity
"""
def read_cards(filename):
	card_data = np.loadtxt(filename)
	list_of_cards = [Set(int(card_data[i][0]),int(card_data[i][1]),int(card_data[i][2]),int(card_data[i][3])) for i in xrange(12)]
	
	return list_of_cards
"""
def play_set(filename):
	data = np.loadtxt(filename,dtype = str)
	combos = list(combinations(data,3))
	length = len(data)
	if length != 12:
		raise ValueError("The file must have 12 cards. You cannot play with fewer or greater than 12 cards.")
	match_vals = []
	entries_True = []
	print len(combos)
	for j in xrange(len(combos)):
		print j
		inputs1 = combos[j][0]
		print inputs1
		inputs2 = combos[j][1]
		#print inputs2
		inputs3 = combos[j][2]
		#print inputs3
		try:
			input1_list = [int(inputs1[i]) for i in xrange(len(inputs1))]
			input2_list = [int(inputs2[i]) for i in xrange(len(inputs2))]
			input3_list = [int(inputs3[i]) for i in xrange(len(inputs3))]

		except:
			raise ValueError("Each character of each line of the input file must be an int.")		
			

		#print len(input1_list),len(input2_list),len(input3_list)

		if len(input3_list) > 4:
			raise ValueError("Each line in your input file should only have 4 characters.")
		elif len(input3_list) < 4:
			raise ValueError("Each line in your input file should only have 4 characters.")

		acceptable_inputs = {0,1,2}
		for i in xrange(4):
			if input3_list[i] not in acceptable_inputs:
				raise ValueError("The only acceptable input values are the int values are '0','1', or '2'.")

		card_1 = Set(input1_list[0],input1_list[1],input1_list[2],input1_list[3])
		card_2 = Set(input2_list[0],input2_list[1],input2_list[2],input2_list[3])
		card_3 = Set(input3_list[0],input3_list[1],input3_list[2],input3_list[3])

		match = bool((card_1.pattern+card_2.pattern+card_3.pattern)%3 == (card_1.color+card_2.color+card_3.color)%3 == (card_1.shape+card_2.shape+card_3.shape)%3 == (card_1.quantity+card_2.quantity+card_3.quantity)%3)
		#print match
		match_vals.append(match)
		if match == True:
			entries_True.append(j)
	return np.sum(match_vals),[combos[m] for m in entries_True]

